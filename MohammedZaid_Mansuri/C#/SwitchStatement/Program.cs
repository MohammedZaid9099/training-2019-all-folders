﻿using System;

namespace SwitchStatement
{
    class Program
    {
        static void Main(string[] args)
        {
            int TotalCost=0;
            Start:
            System.Console.WriteLine("Please Choose your coffee");
            System.Console.WriteLine("1.Small 2.Medium 3.Large");
            int usrChoice=int.Parse(Console.ReadLine());
            
           
            switch (usrChoice)
            {
                case 1:
                    TotalCost+=1;
                    break;
                case 2:
                    TotalCost+=2;
                    break;
                case 3:
                    TotalCost+=3;
                    break;
                default:
                    System.Console.WriteLine("Your Choice is Invalid:-"+usrChoice);
                    goto Start;
            }
            
            
            Decide:
            System.Console.WriteLine("Do you want to buy another coffee- Yes/No");
            string usrDecision=Console.ReadLine();
            switch (usrDecision.ToUpper())
            {
                case "YES":
                    goto Start;
                case "NO":
                    break;
                default:
                    System.Console.WriteLine("Please Provide with Yes or No");
                    goto Decide;
            }




            System.Console.WriteLine("Thank You For Shopping with Us!");
            System.Console.WriteLine("Your Total Bill is:-"+TotalCost+"$");
        }
    }
}
