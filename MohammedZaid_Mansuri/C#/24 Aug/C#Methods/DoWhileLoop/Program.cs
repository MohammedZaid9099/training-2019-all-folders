﻿using System;

namespace DoWhileLoop
{
    class Program
    {
        static void Main(string[] args)
        {
            String userChoice;
            do{
            System.Console.WriteLine("Please Enter The Number");
            int userTarget=int.Parse(Console.ReadLine());
            int Start=0;
            while(Start<= userTarget)
            {
                System.Console.WriteLine(Start);
                Start+=2;
            }
            do
            {
                System.Console.WriteLine("Do you want to continue- Yes/No?");
                userChoice=Console.ReadLine().ToUpper();
                if (userChoice!= "YES" && userChoice != "NO")
                {
                    System.Console.WriteLine("Invalid Choice, Please say Yes or No");
                }
            } while (userChoice!="YES" && userChoice!="NO");
            }while(userChoice=="YES");
        }
    }
}
