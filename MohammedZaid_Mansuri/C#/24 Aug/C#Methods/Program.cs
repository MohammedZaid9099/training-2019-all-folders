﻿using System;

namespace C_Methods
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Welcome To the functions");
            System.Console.WriteLine("Please Enter Number1:-");
            int number1=int.Parse(Console.ReadLine());
            System.Console.WriteLine("Please Enter Number2:-");
            int number2=int.Parse(Console.ReadLine());
            Program p= new Program();
            System.Console.WriteLine("The Adition of two number is:-"+p.AddNumbers(number1,number2));
            System.Console.WriteLine("The Substraction of two number is:-"+p.SubNumbers(number1,number2));
            System.Console.WriteLine("The Multiplication of two number is:-"+p.MulNumbers(number1,number2));
            System.Console.WriteLine("The Division of two number is:-"+p.DivNumbers(number1,number2));
        }
        
        
            public int AddNumbers(int num1,int num2)
            {
                return num1+num2;
            }
            
            public int SubNumbers(int num1,int num2)
            {
                return num1-num2;
            }
            
            public int MulNumbers(int num1,int num2)
            {
                return num1*num2;
            }
            
            public int DivNumbers(int num1,int num2)
            {
                return num1/num2;
            }
    }
}
