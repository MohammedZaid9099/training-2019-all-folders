﻿using System.Globalization;
using System;

namespace For_ForEachLoops
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] Number= new int[]{1,2,3,4,5,6,7,8,9,10};
           
            System.Console.WriteLine("----Array Data Display With For Loop----");
            for (int i = 0; i < Number.Length; i++)
            {
                System.Console.WriteLine(Number[i]);
            }
            System.Console.WriteLine("----Array Data Display ForEach Loop----");
            foreach (var item in Number)
            {
                System.Console.WriteLine(item);
            }
            System.Console.WriteLine("----Break Statement In Loop----");
            foreach (var data in Number)
            {
                System.Console.WriteLine(data);
                if(data==5)
                break;
            }
            System.Console.WriteLine("----Printing Even Numbers with Continue Statement");
            foreach (var even in Number)
            {
                if(even %2 ==1)
                continue;
                System.Console.WriteLine(even);
            }
        }
    }
}
