//normal js function

function greetMessage(name)
{
    console.log("Hello  " + name);
}
greetMessage("Rajat");



//Arrow Function  in ES6
const greetHello=(name)=>{
    console.log("Hello "+name+"  with Arrow Function")
}
greetHello("Zaid");



//you can ommit parenthesis and curly braces if there is one paremeter and one line operation
const greetUser=name=>console.log("Hello "+name+"Single parameter arrow function");

greetUser("Rajat");