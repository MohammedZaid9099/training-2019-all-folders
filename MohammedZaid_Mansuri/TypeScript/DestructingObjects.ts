interface IndiaPostaLAddress{
    streetAddress1:string;
    streetAddress2:string;
    city:string;
    zip:string;
    country:string;
}

const addressDetails: IndiaPostaLAddress={
    streetAddress1:'2974 Dhalgarwad',
    streetAddress2:'Ayasha Manzil',
    city:'Ahmedabad',
    zip:'380001',
    country:'India'
}

const {streetAddress1:street1,streetAddress2:street2,city:city,zip:zip,country:country}=addressDetails;
console.log(street1,street2,city,zip,country);