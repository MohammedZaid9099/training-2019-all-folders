//String templates in ES6


const names=['Zaid','Rajat','Aravinda'];

for(let i=0;i<names.length;i++)
{
    
    console.log(`Hello, ${names[i]}`);
}


// Function using string templates
function NumberOperation(num1,num2)
{
    console.log(`
    The sum of two number is ${num1 + num2}.
    The substraction of two number is ${num1-num2}.
    THe multiplication of two number is ${num1*num2}.
    The division of two number is ${num1/num2}.
    `)
}
NumberOperation(10,2);



//Tagged String templates

let taggedString=function(strArray,...val)
{
    console.log(strArray);
    console.log(val);
}
let number1=10;
let number2=2
taggedString`The addition of ${number1} and ${number2} gives ${number1+ number2}.`


//StartsWith and EndsWith function


let StringStartsWith=function(string)
{
    if(string.startsWith('Zai'))
    {
        console.log(`Your string starts with Zai.`);
    }
    else
    {
        console.log(`Your string does not starts with Zai.`);
    }

};
let string="Zaid Mansuri";
StringStartsWith(string);


let StringEndsWith=function(string)
{
    if(string.endsWith('uri'))
    {
        console.log(`Your String Ends with uri.`);
    }  
    else
    {
        console.log(`Your String does not Ends with uri.`);
    }
};
StringEndsWith(string);


//Includes
let StringIncludes=function(string){
    if(string.includes('Raj'))
    {
        console.log(`Your string includes Raj`);
    }
    else
    {console.log(`Your string doesnot includes Raj`);}

}
StringIncludes(string);

//Repeat Function

//console.log(string.repeat(3));

//Raw Function 

// let raw =String.raw`This is a string which omits new line:\n`;
// console.log(raw);