class NewPerson{
    constructor(name)
    {
        console.log("My name is:-"+name);
    }
    getId()
    {
        return 15;
    }
}

class Employee extends NewPerson
{
    constructor(name){
        super(name);
        console.log("This is an Employee Constructor");
    }
    getId()
    {
        console.log("Employee ID in child class is 16");
        return super.getId();
    }
}

let e = new Employee("Zaid");
console.log("Employee ID is:-"+(e.getId()));
