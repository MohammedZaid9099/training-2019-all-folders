class Person{
    constructor(name,address,city)
    {
        this.name=name;
        this.address=address;
        this.city=city;
    }
    setDetails(name,address,city)
    {
        this.name=name;
        this.address=address;
        this.city=city;
    }
    printDetails()
    {
        console.log("The name is:-"+this.name+",The address is:-"+this.address+",The city is:-"+this.city);
    }
    static staticMethod()
    {
        console.log("This is a static Method");
        
    }
}

let p= new Person();
p.setDetails("Zaid","Khanpur","Ahmedabad");
p.printDetails();
Person.staticMethod();