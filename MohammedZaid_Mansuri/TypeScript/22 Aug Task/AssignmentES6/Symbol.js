let s1 = Symbol("First Symbol");
console.log(typeof s1);
console.log(s1.toString());


console.log(("---Comparing two symbols with same information---"));
let s2=Symbol("Test");
let s3= Symbol("Test");
console.log(s2===s3);


console.log("----Symbol.for() method----");
let s4=Symbol.for("RegisterSymbol");
let s5=Symbol.for("RegisterSymbol");
console.log(s4===s5);

console.log("----Key Symbol For registered Data----");
// console.log(Symbol.keyFor(s1));
// console.log(Symbol.keyFor(s2));
// console.log(Symbol.keyFor(s3));
console.log(Symbol.keyFor(s4));
console.log(Symbol.keyFor(s5));

console.log("---Static Method for Symbol---");

let fname=Symbol("AJAY");
let person={
    [fname]:"Zaid"
}
console.log(Object.getOwnPropertyNames(person));
console.log(Object.getOwnPropertySymbols(person));
