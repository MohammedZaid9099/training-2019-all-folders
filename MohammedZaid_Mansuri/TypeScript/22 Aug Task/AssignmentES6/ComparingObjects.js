let name1={name:'Zaid'};
let name2={name:'Rajat'};

console.log("---Before Reassigning---");
console.log(name1==name2);
console.log(name1===name2);

console.log("---After Assigning---")
let name3=name1;
console.log(name1==name3);
console.log(name1===name3);



console.log("---For Numbers---")
let num=1;
let num1=1;

console.log(num==num1);
console.log(num===num1);