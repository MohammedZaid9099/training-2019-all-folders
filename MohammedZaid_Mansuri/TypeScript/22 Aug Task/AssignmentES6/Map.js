var map=new Map();
map.set('name','Sigma Infosolutions');
map.set('age',23);
map.set(1,'college');
console.log(map.get('age'));
console.log("---Map Values---");
console.log(map.values());

console.log("---Map Keys---");
console.log(map.keys());

console.log("---Map Entries---");
console.log(map.entries());


console.log("---Name Available in Map---");
console.log(`Hello ${map.get('name')}`);


var hasMap=new Map();
map.set(1,true);
console.log(map.has(1));
console.log(map.has("1"));




console.log('----Use Strict----');
'use Strict'

var roles= new Map([
    ['role1','Admin'],['role2','User'],
    ['rolde3','Guest']
]);
for (let role of roles.entries())
{
    console.log(role[0]+" : "+ role[1]);
}