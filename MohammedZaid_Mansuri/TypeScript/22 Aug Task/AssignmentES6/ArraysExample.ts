console.log("---------String Arrays----------");
let alphas;
alphas=["0","1","2","3","4"];
console.log(alphas[0]);
console.log(alphas[1]);
console.log(alphas[2]);
console.log(alphas[3]);
console.log(alphas[4]);
console.log("---------Number Arrays----------");
let nums=[1,2,3,4];
console.log(nums[0]);
console.log(nums[1]);
console.log(nums[2]);
console.log(nums[3]);


console.log("---------Object Arrays----------");
var arrayNames=new Array(5);
for(var i=0;i<arrayNames.length;i++)
{
    console.log(arrayNames[i]=i+2);

}
console.log("---------Constructor Arrays----------");

var conArray=new Array("Zaid","Rajat","Aravinda","Chetan");
for(let i=0;i<conArray.length;i++)
{
    console.log(conArray[i]);
}