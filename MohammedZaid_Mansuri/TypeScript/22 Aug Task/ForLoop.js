//var in for loop
function printNumbers(num) {
    console.log("----Var keyword Loop----");
    for (var i = 1; i <= num; i++) {
        console.log(i);
    }
    console.log(i); //It will work with var 
    for (var i = 1; i <= num; i++) {
        console.log(i);
    }
}
printNumbers(3);
//let in for loop
function showData(newData) {
    console.log("----Let keyword Loop----");
    for (var i = 1; i <= newData; i++) {
        console.log(i);
    }
    // console.log(i); //It wont work with let
    for (var i = 1; i <= newData; i++) {
        console.log(i);
    }
}
showData(3);
