//String templates in ES6
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
var names = ['Zaid', 'Rajat', 'Aravinda'];
for (var i = 0; i < names.length; i++) {
    console.log("Hello, " + names[i]);
}
// Function using string templates
function NumberOperation(num1, num2) {
    console.log("\n    The sum of two number is " + (num1 + num2) + ".\n    The substraction of two number is " + (num1 - num2) + ".\n    THe multiplication of two number is " + num1 * num2 + ".\n    The division of two number is " + num1 / num2 + ".\n    ");
}
NumberOperation(10, 2);
//Tagged String templates
var taggedString = function (strArray) {
    var val = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        val[_i - 1] = arguments[_i];
    }
    console.log(strArray);
    console.log(val);
};
var number1 = 10;
var number2 = 2;
taggedString(__makeTemplateObject(["The addition of ", " and ", " gives ", "."], ["The addition of ", " and ", " gives ", "."
    //StartsWith and EndsWith function
]), number1, number2, number1 + number2);
//StartsWith and EndsWith function
var StringStartsWith = function (string) {
    if (string.startsWith('Zai')) {
        console.log("Your string starts with Zai.");
    }
    else {
        console.log("Your string does not starts with Zai.");
    }
};
var string = "Zaid Mansuri";
StringStartsWith(string);
var StringEndsWith = function (string) {
    if (string.endsWith('uri')) {
        console.log("Your String Ends with uri.");
    }
    else {
        console.log("Your String does not Ends with uri.");
    }
};
StringEndsWith(string);
//Includes
var StringIncludes = function (string) {
    if (string.includes('Raj')) {
        console.log("Your string includes Raj");
    }
    else {
        console.log("Your string doesnot includes Raj");
    }
};
StringIncludes(string);
//Repeat Function
//console.log(string.repeat(3));
//Raw Function 
// let raw =String.raw`This is a string which omits new line:\n`;
// console.log(raw);
