enum Gender{
    Male,
    Female
}


interface People
{
    name:String,
    age:number,
    gender:Gender,
}

let employee:People[]=[
    {
        name:"Zaid",age:23,gender:Gender.Male
    },
    {
        name:"Rajat",age:23,gender:Gender.Male
    },
    {
        name:"Krina",age:30,gender:Gender.Female
    },
    {
        name:"Aastha",age:64,gender:Gender.Female
    },
    
    {
        name:"Aravinda",age:50,gender:Gender.Male
    },
    {
        name:"Chetan",age:25,gender:Gender.Male
    },
    {
        name:"Anshu",age:20,gender:Gender.Male
    },
];



//Fliter
const filterByAge=(employee:People[],range:{min:number,max:number})=>
employee.filter(employee=>employee.age>=range.min && employee.age<range.max);
const minMax={min:23,max:50};
const filteredByAge=filterByAge(employee,minMax);
console.log("----Filter By Age----");
console.log(filteredByAge);


//Sort the elements by age

const sortByAge=(employee:People[])=>
employee.sort((personA:People,personB:People) =>
{
    if(personA.age>personB.age) return 1;
    if(personA.age<personB.age) return 0;
    return 0;
}
);
const sortedByAge=sortByAge(employee);
console.log("-----Sorted By Age-----");
console.log(sortedByAge);

// Filter And Sort By Age

const result= sortByAge(filterByAge(employee,minMax));
console.log("----Filtered and Sortd By Age Ascending----");
console.log(result);
