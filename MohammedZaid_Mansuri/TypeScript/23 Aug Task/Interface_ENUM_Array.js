var Gender;
(function (Gender) {
    Gender[Gender["Male"] = 0] = "Male";
    Gender[Gender["Female"] = 1] = "Female";
})(Gender || (Gender = {}));
var employee = [
    {
        name: "Zaid", age: 23, gender: Gender.Male
    },
    {
        name: "Rajat", age: 23, gender: Gender.Male
    },
    {
        name: "Krina", age: 30, gender: Gender.Female
    },
    {
        name: "Aastha", age: 64, gender: Gender.Female
    },
    {
        name: "Aravinda", age: 50, gender: Gender.Male
    },
    {
        name: "Chetan", age: 25, gender: Gender.Male
    },
    {
        name: "Anshu", age: 20, gender: Gender.Male
    },
];
//Fliter
var filterByAge = function (employee, range) {
    return employee.filter(function (employee) { return employee.age >= range.min && employee.age < range.max; });
};
var minMax = { min: 23, max: 50 };
var filteredByAge = filterByAge(employee, minMax);
console.log("----Filter By Age----");
console.log(filteredByAge);
//Sort the elements by age
var sortByAge = function (employee) {
    return employee.sort(function (personA, personB) {
        if (personA.age > personB.age)
            return 1;
        if (personA.age < personB.age)
            return 0;
        return 0;
    });
};
var sortedByAge = sortByAge(employee);
console.log("-----Sorted By Age-----");
console.log(sortedByAge);
// Filter And Sort By Age
var result = sortByAge(filterByAge(employee, minMax));
console.log("----Filtered and Sortd By Age Ascending----");
console.log(result);
