//Addition of array values using reduce
var vals = [2, 5, 9, 1, 6];
console.log(("----Original Array----"));
console.log(vals);
console.log("----Addition of Array Elements----");
var answer = vals.reduce(function (acc, val) { return acc + val; });
console.log("The addition of [" + vals + "] is " + answer);
console.log("----Max Value in Array----");
var maxValue = vals.reduce(function (acc, val) { return acc > val ? acc : val; });
console.log("The Max Value in [" + vals + "] is " + maxValue);
