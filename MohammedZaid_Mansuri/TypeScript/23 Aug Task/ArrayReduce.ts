//Addition of array values using reduce
let vals=[2,5,9,1,6];

console.log(("----Original Array----"));
console.log(vals);

console.log("----Addition of Array Elements----");
let answer= vals.reduce((acc,val)=> acc+val);
console.log("The addition of ["+vals+"] is "+answer);

console.log("----Max Value in Array----");

let maxValue=vals.reduce((acc,val)=> acc>val ? acc:val);
console.log("The Max Value in ["+vals+"] is "+maxValue);
