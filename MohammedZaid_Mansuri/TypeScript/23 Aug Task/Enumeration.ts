enum EyeColor{Brown=1,Blue=5,Black=7};
console.log(EyeColor);
var myEyeColor=EyeColor.Brown;
console.log(("---------------"));

console.log("Enum value of Brown is:-"+myEyeColor);
console.log("My Eye Color is "+EyeColor[myEyeColor]);

console.log(("---------------"));
myEyeColor=EyeColor.Blue;
console.log("Enum value of Blue is:-"+myEyeColor);
console.log("Rajat's Eye Color is "+EyeColor[myEyeColor]);

console.log(("---------------"));
myEyeColor=EyeColor.Black;
console.log("Enum value of Black is:-"+myEyeColor);
console.log("Aravinda's Eye Color is "+EyeColor[myEyeColor]);