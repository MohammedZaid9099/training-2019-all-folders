interface Person
{
    name:String,
    lname:String,
    age?:number,
}

let employee1:Person=
{
    name:"Zaid",
    lname:"Mansuri",
    age:23
};
let employee2:Person=
{
    name:"Rajat",
    lname:"Agrawal",
};

console.log("Employee 1 detais:-"+employee1.name+" , "+ employee1.lname+" , "+employee1.age);
console.log("Employee 2 detais:-"+employee2.name+" , "+ employee2.lname+" , "+employee2.age);
