document.write("<h1>Array Operations</h1>");
var array=["Zaid","Rajat","Janki"];
document.write("<h2>Original Array</h2>");
for(var i=0;i<array.length;i++)
{
    document.write(array[i]+"<br>");
}


array.shift();
document.write("<h2>Array After Shift Method(Remove 1st element of array)</h2>");
for(var i=0;i<array.length;i++)
{
    document.write(array[i]+"<br>");
}

array.unshift("Zaid");
document.write("<h2>Array After UnShift Method (Add elemet to 1st Index)</h2>");
for(var i=0;i<array.length;i++)
{
    document.write(array[i]+"<br>");
}
array.push("Aravinda");
document.write("<h2>Array After Push Method (Add elemet to last index)</h2>");
for(var i=0;i<array.length;i++)
{
    document.write(array[i]+"<br>");
}
array.pop();
document.write("<h2>Array After Pop Method (Remove elemet from last index)</h2>");
for(var i=0;i<array.length;i++)
{
    document.write(array[i]+"<br>");
}