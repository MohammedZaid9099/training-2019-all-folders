var Employee=["Zaid","Rajat","Janki","Aravinda"];

for(var i=0;i<Employee.length;i++)
{
    document.write("Name of Employee:-"+ Employee[i]);
    document.write("<br>")
}
document.write("<h2>For Each Loop</h2>");
var fruits=["Orange","Apple","Kiwi","Pear"];
fruits.forEach(myFunction);
function myFunction(item,index)
{
    document.write(index+":"+item+"<br>");
}
document.write("<h2>For in Loop</h2>");

var cars=["Volvo","BMW","Mercedes","Lexus"];
for(var index in cars)
{
    document.write(cars[index]+"<br>");
}