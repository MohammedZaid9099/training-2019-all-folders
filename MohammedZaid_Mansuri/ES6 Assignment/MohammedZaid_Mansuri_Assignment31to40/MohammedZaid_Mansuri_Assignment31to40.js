//31. Write a JavaScript program to get the last element from an given array. 
var arr=['Zaid','Rajat','Anshu','Chetan','Janki'];
let x=arr.pop();
console.log(x);


//Assignment 32 Write a JavaScript program to join all elements of an array into a string and returns this string. Uses a separator and an end separator. 
var joinArr=arr.join(',');
console.log("The array elements join with separator:-"+joinArr);
var joinArrEnd=arr.join('.');
console.log("The array elements join with end separator:-"+joinArrEnd);

// Assignment 34 Write a JavaScript program to check if a given string is upper case or not. 
var FullName="ZAIDMANSURI";
if(FullName === FullName.toUpperCase())
{
    console.log("The String is in UpperCase");
    
}
else
{
    console.log("The String is not in LowerCase");    
}