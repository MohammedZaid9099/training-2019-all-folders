var element=document.getElementById('myBtn');
element.addEventListener('mouseover',MouseOverFunction);
element.addEventListener('click',MouseClickedFunction);
element.addEventListener('mouseout',MouseOutFunction);

function MouseOverFunction()
{
    document.getElementById('demo').innerHTML+="Mouse Over! <br>";
}

function MouseClickedFunction()
{
    document.getElementById('demo').innerHTML+="Mouse Clicked! <br>";
}

function MouseOutFunction()
{
    document.getElementById('demo').innerHTML+="Mouse Out! <br>";
}