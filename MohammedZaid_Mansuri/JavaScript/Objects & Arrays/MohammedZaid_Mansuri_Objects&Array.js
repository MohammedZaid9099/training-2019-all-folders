var car={
    make:"volvo",
    speed:160,
    engine:
    {
        size:2.0,
        make:"BMW",
        fuel:"Petrol",
        pistons:[{makers:"BMW"},{makers:"BMW2"}]
    },
    drive:function(){return "drive"}
};


var array=[
    "Zaid",
    100,
    ["embedded",200],
    {car: "ford"},
    function(){return "drive"}
]

document.write("<h1>Car Manufactureres</h1>");
document.write(car.make);
document.write("<br>");
document.write(car.speed);
document.write("<br>");
document.write(car.engine.size);
document.write("<br>");
document.write(car.engine.make);
document.write("<br>");
document.write(car.engine.fuel);
document.write("<br>");
document.write(car.engine.pistons[0].makers);

document.write("<br>");
document.write(car.engine.pistons[1].makers);
document.write("<br>");
console.log(car.engine.pistons[0]);
console.log(car.engine.pistons[1]);

console.log(car);


document.write("<br>");
document.write("<h2>Array Functions</h2>")

document.write(array[0]);
document.write("<br>");
document.write(array[1]);
document.write("<br>");
document.write(array[3].car)