$(function(){
    $("#books_div>.book").on('click','button',function(event){
        var price=$(this).closest(".book").data("price");
        var author=$(this).closest(".book").data("author");
        var details=$("<p>Price:"+price+"<br> Author:<b>"+author +"</b></p>");
        $(this).after(details);
        $(this).closest(".book").css({"background":"yellow",});
        $(this).remove();
        event.stopPropagation();
    });
    $("div.book").on('click',function()
    {
        alert("Click on the button to see details");
    });
    $("#book_completed>.book").on('click','button',function(event)
    {
        var purchased=$(this).closest(".book").data("purchase");
        var author=$(this).closest(".book").data("author");
        var myRating=$(this).closest(".book").data("myrating");
        var details=$("<p>Date:"+purchased+"<br> Author:<b>"+author +"</b> <br> Ratings:"+ myRating+"</p>");
        $(this).after(details);
        $(this).closest(".book").css({"background":"yellow",});
        $(this).remove();
        event.stopPropagation();
    })  
})