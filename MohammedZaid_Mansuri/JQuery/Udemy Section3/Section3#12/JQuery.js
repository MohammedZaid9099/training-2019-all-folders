$(function()
{   
    //children of ul with li tags
    console.log($("#courses").children("li"));

    //nested children of li
    console.log($("#courses").children("li").children());

    //nested children of li inside li
    console.log($("#courses").children("li").children().children());

    //first child of ul or li
    console.log($("#courses").children("li").children().first());

    //last child of ul or li
    console.log($("#courses").children("li").children().last());

    //parent of child
    console.log($("#courses").children("li").children().parent());
    //parents of child
    console.log($("#courses").children("li").children().parents());

    //next selector
    console.log($("#courses").children("li").next());
    console.log($("#courses").children("li").next(),next().next());


    //prev selector
    console.log($("#courses").children("li").next().next().prev());

});