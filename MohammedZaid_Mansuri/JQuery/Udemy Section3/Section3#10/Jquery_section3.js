$(function()
{
    //Targeting all elements of ul
    $("#ulselector li").css("color","blue");

    //Targeting children of ul
    $("#childul>li").css("color","red");
    //Targeting First Child of ul
    $("#childul li:first").css("color","green");
    //Targeting last child of ul
    $("#childul li:last").css("border","1px solid black");
    //Targeting child on nth position
    $("#childul li:nth-child(3)").css("border","2px solid green");


    //Targeting elements based on classes and id

    $("p[class]").css({
        "color":"red",
        "border":"3px dashed gray",
        "background":"yellow"
    });

    $("p[id]").css({
        "font-size":"40px",
        "color":"lightblue",
        "border":"2px solid green"
    });
});