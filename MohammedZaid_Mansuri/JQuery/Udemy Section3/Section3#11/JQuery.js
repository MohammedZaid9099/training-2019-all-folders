$(function(){
    //All Sibling of UL
    $("ul#list_important~div").css("border","2px solid black");

    //Spceiic sibling of Ul
    $("ul#list_important +div").css({
        "color":"green",
        "background":"yellow",
        "border":"2px dashed green"
    });

    //Targeting element which do not contain class
    $("p[class!='important_p']").css({
        "color":"red",
        "background":"yellow",
    });

    //class Starting from letter imp
    $("[class^='imp']").css({
        "font-size":"20px",
        "color":"brown",
    });

    //class ending with some letter
    $("[class$='para']").css({
        'color':'gray',
        'border':'1px solid green',
    });

    // class containing specific value in it
    $("[class~='imppara']").css({
        "border":"none",
        "background":"pink",
    });

    //class values containing letters in it
    $("[class*='aid']").css({
        "color":"lightgreen",
        "background":"red",
        "border":"none",
    });
    

    //Targeting element which has some specific element
    $("div:has(p)").hide();
    $("div:has(p)").fadeIn(1000);

    //Targeting p element which have header tag
    $("p:has(:header)").hide();
    $("p:has(:header)").slideDown(1000);

});